/*
    Webpack config for development environment
    Created by Ivan Wong at march 23, 2015
*/

var webpack = require("webpack");
var path = require("path");
var BowerWebpackPlugin = require("bower-webpack-plugin");

module.exports = {
    output: {
        publicPath: '/assets/',
        filename: 'app.bundle.js',
        chunkFilename: "[id].app.bundle.js"
    },
    resolveLoader: {
        modulesDirectories: ['web_loaders', 'web_modules', 'node_loaders', 'node_modules', '../..']
    },
    module:{
        /* loaders lists */
        loaders: [
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.less$/, loader: "style!css!less"},
            { test: /\.jade$/, loader: 'jade-loader' },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' },
            { test: /\.js$/, exclude: [/node_modules/, /bower_components/], loader: 'babel-loader'},
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
            },
            { test: /\.woff([\?]?.*)$/,  loader: "url-loader?limit=10000&mimetype=application/font-woff&&name=[name]-[hash].[ext]" },
            { test: /\.woff2([\?]?.*)$/,  loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[name]-[hash].[ext]" },
            { test: /\.ttf([\?]?.*)$/,  loader: "url-loader?limit=10000&mimetype=application/octet-stream&name=[name]-[hash].[ext]" },
            { test: /\.otf([\?]?.*)$/,  loader: "url-loader?limit=10000&mimetype=application/x-font-ttf&name=[name]-[hash].[ext]" },
            { test: /\.eot([\?]?.*)$/,  loader: "url-loader?limit=10000&name=[name]-[hash].[ext]" },
            { test: /\.svg([\?]?.*)$/,  loader: "url-loader?limit=10000&mimetype=image/svg+xml&name=[name]-[hash].[ext]" }
        ]
    },
    /* DEBUG FEATURE */
    devtool: "#inline-source-map",
    resolve: {
        extensions: ['', '.js']
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new BowerWebpackPlugin({
            modulesDirectories: ['./src/bower_components'],
            manifestFiles: ['bower.json', '.bower.json'],
            includes: /.*/
            //excludes: /.*\.less$/
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};
