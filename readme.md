# Test task as a simple shop for ServiceWhale by Ivan Wong

### How to build
run follow commands in your terminal:
```sh
$ cd simpleshop
$ npm install gulp -g && npm install webpack -g && npm install bower -g
$ npm install && bower install
```
Next, for build project in development environment just type end press [enter]
```sh
$ gulp
```
And for production env
```sh
gulp production
```

