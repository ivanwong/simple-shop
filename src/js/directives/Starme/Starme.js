import template from './template.jade';

/**
 * Directive for simple rating
 * WARNING: Directive is template depended
 */
export default class Starme {
	constructor($rootScope, tpl) {
		this.restrict= 'AE';
		this.template = tpl;
		this.scope = {
			rate: '@',
			pid: "@"
		};

		this.link = (scope, elem, attrs)=> {

			let ul = angular.element(elem.children()[0]);

			let unStarAll = ()=> {
				let li_s = ul.children();
				for(let i = 0; i < li_s.length; i++) {
					angular.element(li_s[i]).removeClass('stared');
				}

			};

			let initRating = (rate)=> {
				let rating = Math.round(Math.abs(rate));

				if(rating == 0) { return; }

				let li_s = ul.children();

				let _counter = 0;

				while(_counter <= li_s.length && _counter <= rating-1) {
					angular.element(li_s[_counter]).addClass('stared');
					_counter++;
				}

			}

			let setRate = (id)=> {
				unStarAll();
				let li_s = ul.children();
				let _counter = 0;
				while(_counter < id) {
					angular.element(li_s[_counter]).addClass('stared');
					_counter++;
				}

			};

			elem.bind('click', (event)=> {
				let id = parseInt(angular.element(event.target).attr('data-id')) || 0;
				setRate(parseInt(id));
				$rootScope.$emit('product-rated', {pid: scope.pid, rate: id})
			});



			initRating(scope.rate);
		}

		this.controller = ($scope)=> {};
	}


	static StarmeFactory($rootScope) {
		return new Starme($rootScope, template);
	}
}

Starme.$inject = ['$rootScope'];
