import 'angular';

import NavBar from './NavBar/NavBar';
import Starme from './Starme/Starme';

/**
 * Module directives
 */
export default angular.module('simpleShop.directives', [])
	.directive('navBar', NavBar.NavBarFactory)
	.directive('starme', Starme.StarmeFactory);
