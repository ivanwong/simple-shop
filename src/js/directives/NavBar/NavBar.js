
import navBarTemplate from './navbar.jade';
import $ from 'jquery';

/**
 * Directive for control NavBar
 */
export default class NavBar {
	constructor($rootScope, AuthService, Cart) {
		this.restrict = 'E';
		this.template = navBarTemplate;

		this.controller = ($scope)=> {
			$scope.authService = AuthService;
			$scope.cart = Cart;

			// logout
			$scope.logoutClick = ()=> {
				$rootScope.$emit('user-logout', {});
				return false;
			}

			$scope.showCart = ()=> {
				$('#dlg-cart').modal('show');
				return false;
			}

		};

	}

	static NavBarFactory($rootScope, AuthService, Cart) {
		return new NavBar($rootScope, AuthService, Cart);
	}
}


NavBar.$inject = [
	'$rootScope',
	'AuthService',
	'Cart'
];
