
import 'angular';
// angular modules (bult-in and third party)
import uiRouter from 'ui-router';

// import application modules
import moduleControllers from './controllers/module-controllers'
import moduleServices from './services/module-services';
import moduleFilters from './filters/module-filters';
import moduleDirectives from './directives/module-directives';

import 'angular-mask';
let ngMask = 'ngMask';

// define app
let simpleShop = angular.module('simpleShop', [
	uiRouter,
	ngMask,
	moduleControllers.name,
	moduleServices.name,
	moduleFilters.name,
	moduleDirectives.name
]);


// import app config and routes
import './app-config';

// bootstraping function
simpleShop.bootstrap = (elem)=> {
		angular.bootstrap(angular.element(elem), ['simpleShop']);
};


export default simpleShop;
