
/**
 * Implementation of Cart
 */
export default class Cart {
	constructor() {
		this.store = [];
	}


	addItem(item) {
		if(!this.exists(item.id)) {
			this.store.push(item);
			return true;
		} else {
			return false;
		}
	}

	exists(id) {
		let status = false;
		this.store.forEach((i)=> {
			if(i.id == id) {
				status = true;
			}
		});

		return status;
	}

	totalSum() {
		let sum = 0;
		this.store.forEach((i)=> {
			sum += parseFloat(i.price)
		});

		return sum;
	}

	static CartFactory() {
		return new Cart();
	}
}
