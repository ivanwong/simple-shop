import 'angular';

import AuthService from './AuthService';
import ngStorage from '../../vendor/ngstorage.js';
import FakeData from './FakeData';
import Cart from './Cart';

/**
 * Module services
 */
export default angular.module('simpleShop.services', [ngStorage.name])
	.factory('AuthService', AuthService.AuthServiceFactory)
	.factory('FakeData', FakeData.FakeDataFactory)
	.factory('Cart', Cart.CartFactory);
