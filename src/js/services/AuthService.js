
import md5 from '../../vendor/md5';

/**
 * AuthService. Provide methods for signin/up actions
 */
export default class AuthService {
	constructor($localStorage) {
		this.storage = $localStorage;
		this.loggetUser = '';
	}

	/**
	 * Check user credentials
	 * @return {Boolean} true if current user is authenticated
	 */
	isAuthenticated() {
		if(this.loggetUser != '' && typeof this.storage.users != 'undefined') {
			let user = this.getUser(this.loggetUser);
			if(user !== false) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Sign in method
	 * @param  {[type]} userData [description]
	 * @return {Boolean} true if user login successful. false if not registered
	 */
	signIn(userData) {
		let users = this.storage.users || false;
		if(!users) {
			// has no any registered users
			return false;
		}

		let user = this.getUser(userData.email);
		if(!user) {
			// this user not registered
			return false;
		}
		// so user registered, need check credentials
		let status = (this.toHash(userData.password) == user.hash) && (userData.email == user.email);
		if (status) {
			this.loggetUser = user.email;
		}

		return status;
	}

	/**
	 * sign out method
	 * @return none
	 */
	signOut() {
		let users = this.storage.users || [];
		for (let k in users) {
			let user = users[k];
			if ( user != null && user.email == this.loggetUser){
				delete users[k];
				break;
			}
		}
		this.loggetUser = '';
	}

	/**
	 * Sign up method
	 * userData should be validated before call this method
	 * @param  {object} userData {email: '', password: '', password_confirm: ''}
	 * @return true is successfully. false if user with provided email already exists
	 */
	signUp(userData) {

		if(this.getUser(userData.email) !== false) {
			// already exists -> failed
			return false;
		}

		let user = {
			email: userData.email,
			hash: this.toHash(userData.password)
		};
		// init persistent storage if not exists
		if(typeof this.storage.users === 'undefined') {
			this.storage.users = [];
		}
		this.storage.users.push(user);

		return true;
	}


	/**
	 * Get user email(login) from persistent storage (currently - local storage)
	 * @param  {string} email user email
	 * @return {object|Boolean} return user object or false if user not found
	 */
	getUser(email) {
		let persistUsers = this.storage.users || [];
		let user = false;
		persistUsers.forEach((u)=> {

			if(u != null && email === u.email) {
				user = u;
				return;
			}
		});

		return user;

	}

	/**
	 * Get md5 hash of string use third party component
	 * WARNING! use md5 and md5 without salt is very bad idea!
	 *
	 * @param  {string} str string needly
	 * @return {string}  hashed string
	 */
	toHash(str) {
		return md5(str);
	}

	static AuthServiceFactory($localStorage) {
		return new AuthService($localStorage)
	}


}

//
AuthService.$inject = [
	'$localStorage'
];
