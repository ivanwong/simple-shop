
/**
 * Service for provide fake data
 */
export default class FakeData {
	constructor($http) {
		this.http = $http;
	}

	load() {
		return this.http.get('/data.json', {});
	}

	static FakeDataFactory($http){
		return new FakeData($http);
	}
}

FakeData.$inject = [
	'$http'
];
