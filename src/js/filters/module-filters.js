import 'angular';

/**
 * Module filters
 */
export default angular.module('simpleShop.filters', [])

	/**
	 * Filter for products
	 */
	.filter('productFilter', ()=> {
		return (items, filterObj)=> {
			if(typeof filterObj === 'undefined') {
				return items;
			}

			var filtered = [];
			var toTimestamp = (date)=> {
				return new Date(date).getTime();
			}
			var price_from = parseFloat(filterObj.price_from);
			var price_to = parseFloat(filterObj.price_to);
			var date_start = toTimestamp(filterObj.issue_start);
			var date_end = toTimestamp(filterObj.issue_end);

			angular.forEach(items, (item)=> {
				let issue = toTimestamp(item.issue);

				// fuck my brain, fuck my ass, fuck them all! #iwong #nightcoding #imtried
				if((item.price >= price_from && item.price <= price_to) && (issue >= date_start && issue <= date_end) && (item.color == filterObj.color || filterObj.color == 'any') && item.inStock == filterObj.inStock) {
					filtered.push(item);
				}
			});
			return filtered;
		};
	})
