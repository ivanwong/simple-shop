
/**
 * Shop controller
 *
 */
export default class ShopController {
	constructor($scope, $rootScope, FakeData, Cart) {

		this.products = [];
		this.availableColors = [ 'red', 'white', 'blue', 'green', 'black', 'yellow', 'any'];

		this.cart = Cart;

		let searchProduct = (id)=> {
			let status = false;
			this.products.forEach((p)=> {
				if(p.id == id) {
					status = p;
					return;
				}
			});

			return status;
		};

		$rootScope.$on('product-rated', (e, data)=> {
			let product = searchProduct(data.pid);
			if(product != false) {
				product.rating = data.rate;
			}
		});

		FakeData.load()
			.success((res)=> {
				this.products = res;
			})
			.error(()=> {alert('Cannot fetch data from backend')});

		this.filterObject = {
			price_from: 0,
			price_to: 500,
			issue_start: '01/01/2015',
			issue_end: '06/30/2015',
			color: 'any',
			inStock: true
		};
	}

	order(item) {
		if(!this.cart.addItem(item)) {
			alert('Product already exists in your cart');
		}
	}
}



ShopController.$inject = [
	'$scope',
	'$rootScope',
	'FakeData',
	'Cart'
];
