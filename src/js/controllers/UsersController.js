
/**
 * UsersController
 */
export default class UsersController {
	constructor($scope, $rootScope, $state, AuthService) {
		this.root = $rootScope;
		this.root.error = '';
		this.signupData = {
			email: '',
			password: '',
			password_confirm: ''
		};

		this.signinData = {
			email: '',
			password: ''
		};

		this.state = $state;

		this.authService = AuthService;
		$rootScope.$on('user-logout', (e)=> {this.signOut()})
	}

	signOut() {
		this.authService.signOut();
		//this.root.$broadcast('user-logout', {});
		this.state.go('signin');
	}


	signIn() {
		// clear any messages from error
		this.root.error = '';
		if(this.signinData.email == '' || this.signinData.password == '') {
			return; // prevent users jokes on my performance
		}
		let status = this.authService.signIn(this.signinData);
		if(status) {
			// broadcast
			this.root.$broadcast('user-logged', {email: this.signinData.email});
			this.state.go('shop');
		} else {
			this.root.error = 'Wrong email or password';
			this.signinData.password = '';
		}
	}

	signUp(valid) {
		if(!valid) {
			this.root.error = 'Please fill correct all required filed.'
			return;
		}
		if(this.validate()) {
			let status = this.authService.signUp(this.signupData);
			if(!status) {
				this.root.error = 'User already registered, please enter another email';
			} else {
				this.state.go('signin');
			}
		}

	}

	validate() {
		let status = true;
		let error = '';
		let email_re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;


		if(!email_re.test(this.signupData.email)) {
			this.root.error = "E-mail isn't valid an email address";

			return false;
		}
		if(this.signupData.password.length < 5) {
			this.root.error = 'Password too weak (min length is 5 characters)';
			status = false;
			return false;
		}

		if(this.signupData.password != this.signupData.password_confirm) {
			this.root.error = "Passwords do not match";
			status = false;
			return false;
		}

		return status;

	}
}


UsersController.$inject = [
	'$scope',
	'$rootScope',
	'$state',
	'AuthService'
];
