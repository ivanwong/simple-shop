import 'angular';


import ShopController from './ShopController';
import UsersController from './UsersController';

/**
 * Module controllers
 */
export default angular.module('simpleShop.controllers', [])
	.controller('ShopController', ShopController)
	.controller('UsersController', UsersController);
