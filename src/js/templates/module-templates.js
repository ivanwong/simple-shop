
let templatesList = {
	shop: require('./jade/shop.jade'),
	signin: require('./jade/signin.jade'),
	signup: require('./jade/signup.jade')
};
export default templatesList;
