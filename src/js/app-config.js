import 'angular';
import tpl from './templates/module-templates.js';

export default angular.module('simpleShop')
	.config([
		'$stateProvider',
		'$urlRouterProvider',
		($stateProvider, $urlRouterProvider)=> {

			$urlRouterProvider.otherwise('/');

			$stateProvider
				/* HOME */
				.state('shop', {
					url: '/',
					template: tpl.shop,
					controller: 'ShopController',
					controllerAs: 'ShopCtrl'
				})
				/* SIGNIN*/
				.state('signin', {
					url: '/signin',
					template: tpl.signin,
					controller: 'UsersController',
					controllerAs: 'UsersCtrl'
				})
				/* SIGNUP */
				.state('signup', {
					url: '/signup',
					template: tpl.signup,
					controller: 'UsersController',
					controllerAs: 'UsersCtrl'
				});
		}
	])
	// protect routes (make shop route available for authenticated user only)
	.run(($rootScope, $state, AuthService)=> {
		$rootScope.$on('$stateChangeStart', (event, toState  , toParams, fromState, fromParams)=> {
			if (!AuthService.isAuthenticated()) {
				if(toState.name != 'signin' && toState.name != 'signup') {
					event.preventDefault();
					$state.go('signin');
				}
				return;
			}
		});
	});
