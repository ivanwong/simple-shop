
import 'angular';

// load all third party componens as a chunk
require.ensure([], (require) => {
	// third party libs
	require('./bower_components/fontawesome/css/font-awesome.css');
	require('bootstrap') // use full package because need js for open modal manually
	//require('./bower_components/bootstrap/dist/css/bootstrap.css'); // Here, i'm use only bootstrap css without js componens
	// bs  custom theme and fixes
	require('./styles/theme.styl');
});




// import angular application
import app from './js/simpleShop';


// manual run application and bind to body alement
angular.element(document).ready(() => {
	app.bootstrap(document.body);
});
