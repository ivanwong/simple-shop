/* GULP TASKS */


var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var webpack = require('gulp-webpack');
var jade = require('gulp-jade');
var runSequence = require('run-sequence');

var wpDevelopConfig = require('./config/wp.dev.config.js');
var wpProductionConfig = require('./config/wp.production.config.js');
var DEVMODE = true;
/**
 * Default task.
 * $> gulp
 * Will run all tasks and activate watch + browser sync
 * This task only dev env and cannot use on production servers
 */
//gulp.task('default', ['dev']);


gulp.task('production', function(cb) {
	DEVMODE = false; // disable develop mode
	runSequence(['templates:jade', 'images:move', 'data:copy', 'webpack'], cb);
});

/* build frontend */
gulp.task('default', function (cb) {
	runSequence(['templates:jade', 'images:move', 'data:copy', 'webpack'], cb);
});

/* Compile jade templates inside src/templates */
gulp.task('templates:jade', function () {
	return gulp.src('./src/templates/**/*.jade')
		.pipe(jade({
			client: false
		}))
		.pipe(gulp.dest('./public/'));
});

/* webpack */
gulp.task("webpack", function () {
	var config = DEVMODE ? wpDevelopConfig : wpProductionConfig;
	return gulp.src('./src/loader.js')
		.pipe(webpack(config))
		.pipe(gulp.dest('./public/assets'));
});

/* move all images to public */
gulp.task('images:move', function () {
	return gulp.src(['./src/img/**/*.*'])
		.pipe(gulp.dest('./public/assets/img'));
});

gulp.task('data:copy', function() {
	return gulp.src('./src/data.json')
		.pipe(gulp.dest('./public/'));
});
